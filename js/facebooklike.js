/**
 * @file
 * Javascript behaviors for the Facebook Likepage module.
 */

(function ($) {
    Drupal.behaviors.facebookLike = {
        attach: function (context, settings) {
            $("#facebook-likepage").hover(function () {
                $(".facebook-like-item").animate({right: '300px'});
            }, function () {
                $(".facebook-like-item").animate({right: '0px'});
            }
            );
        }
    }
})(jQuery);
