-- SUMMARY --

Simple module provides a Facebook like with the latest updates from Facebook for
the provided account. It is based on the likebox social plugin:
http://developers.facebook.com/docs/reference/plugins/like-box. The widget
settings are configurable directly in the system configuration and they are 
available for users with 'administer site configuration' permission.


-- REQUIREMENTS --

* None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
* Or put folder to the directory modules, then go to "admin/modules" and enable 
module: "Facebook Like Page". 

You also can use drush: "drush en facebook_likepage"


-- CONFIGURATION --

* Go to Site > admin > config
* The Facebook Like Page in User Interface
* Click on 'Facebook Like Page':
* Add the Facebook Page (i.e.: http://www.facebook.com/wikisaber.es) and
  configure the display and appearance settings.

 - Configuration examples:

 -- Ebable: to ebable this plugin that allow show or hide
 -- Url: link url of the facebook page
 -- Show Friend's Faces: yes > Show friends that liked your Facebook page
 -- Hide Cover Photo: Yes >  hide cover photo of the Facebook page
 -- Show Page Posts: yes > Show your posts that you posted in your facebook page
 -- Width: set with for plugin
 -- Height: set height for plugin
 -- Exclude: enter urls that you want to exclude the plugin

-- CONTACT --

nhanlego1@gmail.com
