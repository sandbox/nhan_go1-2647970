<?php

/**
 * @file
 * Default theme implementation to show facebook likepage icon.
 *
 * Presented under nodes that are a part of book outlines.
 */

?>

<div id="facebook-likepage" class="facebook-likepage-wrapper">
  <div class="facebook-like-item">
    <span class="fbspan"></span>
    <div class="fb-page" <?php print drupal_attributes($facebook['data_attributes']);?></div>
  </div>
</div>
<div id="fb-root"></div>
<script>
  (function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
      return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/<?php print $facebook['language'] ?>/sdk.js#xfbml=1&version=v2.3";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
