<?php

/**
 * @file
 * Facebook Like page administration pages.
 */

/**
 * Returns with the general configuration form.
 */
function facebook_likepage_admin_settings($form, &$form_state) {
  $form['facebook_likepage_status'] = array(
    '#title' => t('Enable Facebook Like Page'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('facebook_likepage_status', FALSE),
  );

  $form['facebook_likepage_facebook_page'] = array(
    '#title' => t('The URL of the Facebook page'),
    '#type' => 'textfield',
    '#default_value' => variable_get('facebook_likepage_facebook_page', 'https://www.facebook.com/FacebookDevelopers'),
    '#description' => t('Please enter facebook page url <br> <p>EX: https://facebook.com/abc</p>'),
    '#require' => TRUE,
  );

  $form['facebook_likepage_friend'] = array(
    '#title' => t("Show Friend's Faces"),
    '#type' => 'select',
    '#default_value' => variable_get('facebook_likepage_friend', 1),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
  );

  $form['facebook_likepage_cover'] = array(
    '#title' => t('Hide Cover Photo'),
    '#type' => 'select',
    '#default_value' => variable_get('facebook_likepage_cover', 1),
    '#options' => array(
      0 => t('Yes'),
      1 => t('No'),
    ),
  );

  $form['facebook_likepage_posts'] = array(
    '#title' => t('Show Page Posts'),
    '#type' => 'select',
    '#default_value' => variable_get('facebook_likepage_posts', 1),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
  );

  $form['facebook_likepage_width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => variable_get('facebook_likepage_width', ''),
    '#description' => t('Set width for facebook like page. Minimum for width is 280'),
  );

  $form['facebook_likepage_height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => variable_get('facebook_likepage_height', ''),
    '#description' => t('Set height for facebook like page. Minimum for height is 130. If choose show Page Posts, Height auto set to empty when load facebook like.'),
  );

  $form['facebook_likepage_exclude'] = array(
    '#type' => 'textarea',
    '#title' => t('Excluded Pages'),
    '#default_value' => variable_get('facebook_likepage_exclude', 'admin/*'),
    '#description' => t('List the page paths to be excluded from Facebook Like Pgae on each line. This is useful when you want to redirect your user with disabled javascript browser on pages with the same content but not using Facebook Like page. Ex. node/1'),
  );

  return system_settings_form($form);
}
